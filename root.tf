provider "aws" {
    region     = "us-east-1"
}

module "cbucket" {
    source = "./modules/bucket"

    tag_name        = "${var.tag_name}"
    tag_environment = "${var.tag_environment}"
    bucket_prefix   = "${var.bucket_prefix}"
    acls            = "${var.acls}"
    policy          = "${var.policy}"
}

