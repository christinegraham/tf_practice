output "id" {
    value = "${join(",",aws_s3_bucket.example_bucket.*.id)}"
}

output "acl" {
    value = "${join(",",aws_s3_bucket.example_bucket.*.acl)}"
}

output "name" {
    value = "${join(",",aws_s3_bucket.example_bucket.*.bucket)}"
}
