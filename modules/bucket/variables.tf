variable "tag_name" {}

variable "tag_environment" {}

variable "bucket_prefix" {}

variable "acls" {
    type = "map"
}

variable "policy" {}

