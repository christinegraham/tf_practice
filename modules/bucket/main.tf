locals {
    resource_count = "${length(var.acls)}"
}

resource "aws_s3_bucket" "example_bucket" {
    count = "${local.resource_count}"
    bucket_prefix = "${var.bucket_prefix}${count.index}"
    acl           = "${lookup(var.acls, count.index)}"
    tags {
        Name        = "${var.tag_name}"
        Environment = "${var.tag_environment}"
    }
}

resource "aws_s3_bucket_policy" "example_bucket_policy" {
    count = "${var.policy ? local.resource_count : 0}"
    bucket = "${aws_s3_bucket.example_bucket.*.bucket[count.index]}"
    policy =<<POLICY
{
  "Id": "Policy1518576604714",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1518576575889",
      "Action": "s3:*",
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.example_bucket.*.arn[count.index]}",
      "Condition": {
        "StringLike": {
          "s3:prefix": "cdog"
        }
      },
      "Principal": "*"
    }
  ]
}
POLICY
}

