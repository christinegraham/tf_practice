output "id" {
    value = "${module.cbucket.id}"
}

output "acl" {
    value = "${module.cbucket.acl}"
}

output "name" {
    value = "${module.cbucket.name}"
}
