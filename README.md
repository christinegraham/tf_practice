## Terraform Practice

This project will create two S3 buckets, apply different acls to each,
and conditionally apply a bucket policy to each bucket.

### To Test

Make sure you have your `~/.aws/credentials` file set with the correct values.

Run:
* `terraform plan` to see what changes will be applied
* `terraform apply` to apply changes

You will be prompted to enter a value for `var.policy`. 
* `true` will apply the bucket policy resource to each bucket
* `false` will not apply the bucket policies

Finally, run:
* `terraform destroy` to clean up your resources
