variable "tag_name" {
    default = "cdogs_bucket"
}

variable "tag_environment" {
    default = "cdog"
}

variable "bucket_prefix" {
    default = "cdog_prefix_"
}

variable "acls" {
    default = {
        "0" = "private"
        "1" = "log-delivery-write"
    }
}

variable "policy" {}

